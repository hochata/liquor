# Liquor

Cheap, yet delicious, elixir

## Wonderland katas
A bunch of Alice in Wonderland inspired tasks. As per intructions, these are a fork on a separate repository available [here](https://codeberg.org/eqf0/wonderland_elixir_katas)

## Phoenix 

+ Some demo applications
+ Also [this pet clinic]( https://codeberg.org/eqf0/pet_clinic)

## Bunsan tasks

A bunch of simple learning exercices

* [Fizzbuzz](https://rosettacode.org/wiki/FizzBuzz)
* Pragmatic Bookshelf Taxes from [this book](https://pragprog.com/titles/elixir16/programming-elixir-1-6/)
* Dummy implementation of some `Enum` functions
* [CDMX Challenge](https://gist.github.com/MachinesAreUs/a28c0173fd0d4a57d534129d9d6bcafb)
* Learn to make unit tests with [ExUnit](https://hexdocs.pm/ex_unit/1.13/ExUnit.html) and [PropCheck](https://hexdocs.pm/propcheck/readme.html)
* Play around with [these macros](https://github.com/MachinesAreUs/elixir_meetup_macros_2017)
* 

## Advent of Code

 A late try for the 2021 AoC programming problems.

* Day 1: Sonnar Sweep
* Day 2: Dive!

## Some Erlangelists examples

* Game of Life example

## Other experiments
* Some dabling into BEAM disassembler
+ Some Datalog experimets
+ Some evolutionary grammars implementations

## Basic RabbitMQ
-  [Some tutorials](https://www.rabbitmq.com/getstarted.html)
- And this example topology
![](./assets/rabbit-ex.png)
