defmodule Aoc.SonarSweep do
  @moduledoc false

  def count_increases(measures, interval) do
    measures
    |> Enum.drop(interval)
    |> Enum.zip(measures)
    |> Enum.count(fn {x, y} -> y < x end)
  end

  def part_one(measures) do
    measures
    |> count_increases(1)
    |> IO.puts()
  end

  def part_two(measures) do
    measures
    |> count_increases(3)
    |> IO.puts()
  end

  def day_one(file_name) do
    measures =
      file_name
      |> File.read!()
      |> String.split("\n")
      |> Enum.map(&Integer.parse/1)

    part_one(measures)
    part_two(measures)
  end
end
