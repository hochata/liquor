defmodule Aoc.Dive do
  @moduledoc false

  def move([direction, value], state) do
    {n, _unparsed} = Integer.parse(value)

    case direction do
      "down" -> %{state | aim: state.aim + n}
      "up" -> %{state | aim: state.aim - n}
      "forward" -> %{state | hor: state.hor + n, depth: state.depth + state.aim * n}
    end
  end

  def day_two(file_name) do
    actions =
      file_name
      |> File.read!()
      |> String.trim()
      |> String.split("\n")
      |> Enum.map(fn s -> String.split(s, " ") end)

    final = Enum.reduce(actions, %{aim: 0, hor: 0, depth: 0}, &move/2)

    IO.puts(final.aim * final.hor)
    IO.puts(final.hor * final.depth)
  end
end
