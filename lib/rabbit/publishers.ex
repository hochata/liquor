defmodule Rabbit.Publisher do
  @exchange_name "rabbit"

  def send(mess) do
    {:ok, conn} = AMQP.Connection.open()
    {:ok, chan} = AMQP.Channel.open(conn)
    AMQP.Exchange.declare(chan, @exchange_name, :fanout)
    AMQP.Basic.publish(chan, @exchange_name, "", mess)
    IO.puts("#{mess} sent!")
    AMQP.Connection.close(conn)
  end

  def receive() do
    {:ok, conn} = AMQP.Connection.open()
    {:ok, chan} = AMQP.Channel.open(conn)
    AMQP.Exchange.declare(chan, @exchange_name, :fanout)
    {:ok, %{queue: queue_name}} = AMQP.Queue.declare(chan, "", exclusive: true)
    AMQP.Queue.bind(chan, queue_name, @exchange_name)
    AMQP.Basic.consume(chan, queue_name, nil, no_ack: true)

    IO.puts("waiting...")
    receive do
      {:basic_deliver, payload, _meta} ->
        IO.puts("got #{payload}!")
    end
    AMQP.Connection.close(conn)
  end
end
