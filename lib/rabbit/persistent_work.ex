defmodule Rabbit.PersistentWork do
  @queue_name "work_queue"

  def send(mess) do
    {:ok, conn} = AMQP.Connection.open()
    {:ok, chan} = AMQP.Channel.open(conn)
    AMQP.Queue.declare(chan, @queue_name, durable: true)
    AMQP.Basic.publish(chan, "", @queue_name, mess, persistent: true)
    IO.puts("#{mess} sent!")
    AMQP.Connection.close(conn)
  end

  def receive() do
    {:ok, conn} = AMQP.Connection.open()
    {:ok, chan} = AMQP.Channel.open(conn)
    AMQP.Queue.declare(chan, @queue_name, durable: true)
    AMQP.Basic.qos(chan, prefetch_count: 1)
    AMQP.Basic.consume(chan, @queue_name)

    IO.puts("waiting...")
    receive do
      {:basic_deliver, payload, meta} ->
        IO.puts("received #{payload}!")
        expensive_call()
        AMQP.Basic.ack(chan, meta.delivery_tag)
    end
    AMQP.Connection.close(conn)
  end

  defp expensive_call() do
    :timer.sleep(1000)
    IO.puts("done!")
  end
end
