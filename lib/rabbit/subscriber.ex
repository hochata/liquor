defmodule Rabbit.Subscriber do
  @exchange_name "pets"

  def send(mess, pet) do
    {:ok, conn} = AMQP.Connection.open()
    {:ok, chan} = AMQP.Channel.open(conn)
    AMQP.Exchange.declare(chan, @exchange_name, :direct)
    AMQP.Basic.publish(chan, @exchange_name, pet, mess)
    IO.puts("sending #{mess}!")
    AMQP.Connection.close(conn)
  end

  def receive(pet) do
    {:ok, conn} = AMQP.Connection.open()
    {:ok, chan} = AMQP.Channel.open(conn)
    AMQP.Exchange.declare(chan, @exchange_name, :direct)
    {:ok, %{queue: queue_name}} = AMQP.Queue.declare(chan, "", exclusive: true)
    AMQP.Queue.bind(chan, queue_name, @exchange_name, routing_key: pet)
    AMQP.Basic.consume(chan, queue_name, nil, no_ack: true)

    IO.puts("waiting...")
    receive do
      {:basic_deliver, payload, _meta} ->
        IO.puts("just got #{payload}!")
    end
    AMQP.Connection.close(conn)
  end
end
