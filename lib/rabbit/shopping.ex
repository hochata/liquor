defmodule RabbitMQ.System do
  require Logger

  @doc """
  Creates the exchange, the queues and their bindings.
  If the exchange and queues already exist, does nothing.
  """
  def setup(exchange_name, queue_names) do
    {:ok, conn} = AMQP.Connection.open()
    {:ok, chan} = AMQP.Channel.open(conn)
    AMQP.Exchange.declare(chan, exchange_name, :direct)
    Enum.each(queue_names, fn queue ->
      AMQP.Queue.declare(chan, queue)
      AMQP.Queue.bind(chan, queue, exchange_name, routing_key: queue)
    end)
    Logger.info("setting up #{exchange_name} with #{queue_names}!")
    AMQP.Connection.close(conn)
  end
end

defmodule RabbitMQ.Producer do
  require Logger

  @doc """
  Sends n messages with payload 'msg' and the given routing key.
  """
  def send(exchange, routing_key, msg, n) when n > 0 do
    {:ok, conn} = AMQP.Connection.open()
    {:ok, chan} = AMQP.Channel.open(conn)
    Enum.each(1..n, fn _ ->
      AMQP.Basic.publish(chan, exchange, routing_key, msg)
    end)
    Logger.info("sending #{msg} to #{routing_key} #{n} times!")
    AMQP.Connection.close(conn)
  end
end

defmodule RabbitMQ.Consumer do
  require Logger

  @doc """
  Creates a process that listens for messages on the given queue.
  When a message arrives, it writes to the log the pid, queue_name and msg.
  Example:
    iex> {:ok, pid} = Consumer.start("orders")
  """
  def start(queue_name) do
    spawn(fn -> wait_for_msgs(queue_name) end)
  end

  defp wait_for_msgs(queue_name) do
    {:ok, conn} = AMQP.Connection.open()
    {:ok, chan} = AMQP.Channel.open(conn)
    AMQP.Basic.consume(chan, queue_name, nil, no_ack: true)
    receive do
      {:basic_deliver, payload, _meta} ->
        IO.puts("#{inspect(self())} just got #{payload} from #{queue_name}!")
        AMQP.Connection.close(conn)
        wait_for_msgs(queue_name)

      :stop ->
        AMQP.Connection.close(conn)
        :ok
    end
  end

  @doc """
  Stops the given consumer.
  Example:
    iex> Consumer.stop("orders")
  """
  def stop(pid) do
    send(pid, :stop)
  end
end
