defmodule Rabbit.Hola do
  @queue_name "hello_queue"

  def send(mess) do
    {:ok, conn} = AMQP.Connection.open()
    {:ok, chan} = AMQP.Channel.open(conn)
    AMQP.Queue.declare(chan, @queue_name)
    AMQP.Basic.publish(chan, "", @queue_name, mess)
    IO.puts("#{mess} sent!")
    AMQP.Connection.close(conn)
  end

  def receive() do
    {:ok, conn} = AMQP.Connection.open()
    {:ok, chan} = AMQP.Channel.open(conn)
    AMQP.Queue.declare(chan, @queue_name)
    AMQP.Basic.consume(chan, @queue_name, nil, no_ack: true)

    IO.puts("waiting...")

    receive do
      {:basic_deliver, payload, _meta} ->
        IO.puts("#{payload} received!")
    end
    AMQP.Connection.close(conn)
  end
end
