defmodule Bunsan.Github do
  use Tesla

  plug Tesla.Middleware.BaseUrl, "https://api.github.com"
  plug Tesla.Middleware.Headers, [{"User-Agent", "Tesla"}]
  plug Tesla.Middleware.JSON

  def get_body(url) do
    url
    |> get()
    |> elem(1)
    |> Map.get(:body)
  end

  def user_repos(user) do
    get_body("/users/" <> user <> "/repos")
    |> Enum.map(& &1["full_name"])
  end

  def user_repo(user, repo) do
    commits_num =
      get_body("/repos/" <> user <> "/" <> repo <> "/commits")
      |> length()

    languages = get_body("/repos/" <> user <> "/" <> repo <> "/languages")

    %{name: repo, commits_num: commits_num, languages: languages}
  end
end
