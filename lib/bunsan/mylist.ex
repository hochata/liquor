defmodule Bunsan.MyList do
  @moduledoc false

  @doc """
  Applied the given function to each element. Note that it will just return an
  status, so it doesn't make much sense to call it with a pure function.

  Examples:

  iex> Bunsan.MyList.each([1, 2, 3], &(&1))
  :ok
  """
  @spec each([in_elem], (in_elem -> any())) :: :ok when in_elem: any()
  def each([], _), do: :ok

  def each([x | xs], f) do
    f.(x)
    each(xs, f)
  end

  @doc """
  Takes a list and build a new list, result of applying the given function to
  every element.

  Examples:

  iex> Bunsan.MyList.map([6, 7, 8], &(&1 * 2))
  [12, 14, 16]

  iex> Bunsan.MyList.map([:h, :o, :l, "a"], &is_atom/1)
  [true, true, true, false]
  """
  @spec map([in_elem], (in_elem -> out_elem)) :: [out_elem]
        when in_elem: any(), out_elem: any()
  def map([], _), do: []
  def map([x | xs], f), do: [f.(x) | map(xs, f)]

  @doc """
  The given reduction function receives the current state and a value, and
  returns an updated state.

  This function applies the reduction function to every element, with a given
  starting state.

  Examples:

  iex> Bunsan.MyList.reduce([1, 2, 3], 0, &+/2)
  6

  iex> Bunsan.MyList.reduce([5, 6, 23], 0, &max/2)
  23

  iex> Bunsan.MyList.reduce(["hola", "m"], "", fn s, st -> st <> " " <> s end)
  " hola m"
  """
  @spec reduce([in_elem], acc, (in_elem, acc -> acc)) :: acc
        when in_elem: any(), acc: any()
  def reduce([], acc, _), do: acc
  def reduce([x | xs], acc, f), do: reduce(xs, f.(x, acc), f)

  @doc """
  Takes two lists and build a list by matching each corresponding element in a
  tuple.

  The length of the resulting list is the length of the shortest given list.

  Examples:

  iex> Bunsan.MyList.zip([:one, :two, :three], [1, 2, 3])
  [{:one, 1}, {:two, 2}, {:three, 3}]
  """
  @spec zip([e1], [e2]) :: [{e1, e2}] when e1: any(), e2: any()
  def zip([], _), do: []
  def zip(_, []), do: []
  def zip([x | xs], [y | ys]), do: [{x, y} | zip(xs, ys)]

  @doc """
  The given function recieves two arguments. Those arguments are taken from the
  two given lists, and the result of each evaluation is used to build a new
  list.

  Examples:

  iex> Bunsan.MyList.zip_with([1, 2, 3], [3, 2, 1], &*/2)
  [3, 4, 3]

  iex>Bunsan.MyList.zip_with([%{o: 1},%{th: 3}],[%{tw: 2},%{f: 4}],&Map.merge/2)
  [%{o: 1, tw: 2}, %{th: 3, f: 4}]
  """
  @spec zip_with([e1], [e2], (e1, e2 -> res)) :: [res]
        when e1: any(), e2: any(), res: any()
  def zip_with([], _, _), do: []
  def zip_with(_, [], _), do: []
  def zip_with([x | xs], [y | ys], f), do: [f.(x, y) | zip_with(xs, ys, f)]

  @doc """
  Splits the given list into a list of tuples, using the value of `f.(x)` as a
  discriminant, preserving the order

  Examples:
  iex> Bunsan.MyList.group_by([1, 2, 2, 2, 1], fn x -> x end)
  [{1, [1]}, {2, [2, 2, 2]}, {1, [1]}]

  """
  @spec group_by([elem], (elem -> key)) :: [{key, elem}]
        when elem: any(), key: any()
  def group_by([], _), do: []

  def group_by([x | xs], f) do
    reduce(xs, [{f.(x), [x]}], fn x, l = [{k, vs} | xs] ->
      case f.(x) == k do
        true -> [{k, [x | vs]} | xs]
        _ -> [{f.(x), [x]} | l]
      end
    end)
    |> Enum.map(fn {k, v} -> {k, Enum.reverse(v)} end)
    |> Enum.reverse()
  end
end
