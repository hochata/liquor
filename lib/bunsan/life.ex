defmodule Life do
  defstruct cells: nil, size: nil

  defp build_cells(size, filter_fn) do
    for x <- 0..(size - 1),
        y <- 0..(size - 1),
        filter_fn.(x, y),
        into: %MapSet{} do
      {x, y}
    end
  end

  def new(size) when is_integer(size) do
    rand_fn = fn _, _ -> :rand.uniform(2) == 1 end
    {:ok, %__MODULE__{cells: build_cells(size, rand_fn), size: {size, size}}}
  end

  def new(cells) when is_list(cells) do
    if Enum.all?(cells, &(is_tuple(&1) and tuple_size(&1) == 2)) do
      hashed_cells = MapSet.new(cells)
      max_x = Enum.max_by(hashed_cells, &elem(&1, 0), fn -> {0, 0} end) |> elem(0)
      max_y = Enum.max_by(hashed_cells, &elem(&1, 1), fn -> {0, 0} end) |> elem(1)

      {
        :ok,
        %__MODULE__{cells: MapSet.new(cells), size: {max_x + 1, max_y + 1}}
      }
    else
      {:error, "Invalid cell contents"}
    end
  end

  def cell_alive?(grid, x, y), do: {x, y} in grid.cells

  def count_nei(grid, x, y) do
    for xp <- (x - 1)..(x + 1), yp <- (y - 1)..(y + 1), reduce: 0 do
      acc -> acc + if {xp, yp} in grid.cells and {xp, yp} != {x, y}, do: 1, else: 0
    end
  end

  def will_be_alive?(grid, x, y) do
    case {cell_alive?(grid, x, y), count_nei(grid, x, y)} do
      {true, 2} -> true
      {true, 3} -> true
      {false, 3} -> true
      _ -> false
    end
  end

  def next(grid) do
    {max_x, max_y} = grid.size

    for x <- 0..max_x,
        y <- 0..max_y,
        will_be_alive?(grid, x, y) do
      {x, y}
    end
    |> new()
    |> elem(1)
    |> Map.put(:size, grid.size)
  end
end
