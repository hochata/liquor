defmodule Bunsan.FizzBuzz do
  @moduledoc false

  @doc """
  If the number is a multiple of 3, return "fizz". Multiple of 5, return "buzz"
  And if it is multiple of both return "fizzbuzz".

  Examples

  iex> Bunsan.FizzBuzz.fizzbuzz_n 1
  1

  iex> Bunsan.FizzBuzz.fizzbuzz_n 27
  "fizz"

  iex> Bunsan.FizzBuzz.fizzbuzz_n 55
  "buzz"

  iex> Bunsan.FizzBuzz.fizzbuzz_n 90
  "fizzbuzz"
  """
  @spec fizzbuzz_n(integer()) :: String.t()
  def fizzbuzz_n(n) do
    cond do
      rem(n, 15) == 0 -> "fizzbuzz"
      rem(n, 5) == 0 -> "buzz"
      rem(n, 3) == 0 -> "fizz"
      true -> n
    end
  end

  @spec fizzbuzz(integer()) :: :ok
  def fizzbuzz(n) do
    Enum.each(1..n, fn i -> IO.puts(fizzbuzz_n(i)) end)
  end
end
