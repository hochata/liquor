defmodule Bunsan.MetroCDMX do
  @moduledoc false

  import SweetXml

  defmodule Station do
    @moduledoc false

    defstruct name: "", coords: {0, 0, 0}

    @type t :: %__MODULE__{
            name: String.t(),
            coords: {number(), number(), number()}
          }
  end

  defmodule Line do
    @moduledoc false

    defstruct name: "", stations: []

    @type t :: %__MODULE__{
            name: String.t(),
            stations: [Station.t()]
          }
  end

  defmodule Segment do
    @moduledoc false

    defstruct line: "", init: "", last: "", len: 0

    @type t :: %__MODULE__{
            line: String.t(),
            init: String.t(),
            last: String.t(),
            len: integer()
          }
  end

  @type coord :: {number(), number(), number()}

  @spec parse_coords(String.t()) :: coord()
  defp parse_coords(s) do
    s
    |> String.trim()
    |> String.split(",")
    |> Enum.map(fn x ->
      if String.contains?(x, ".") do
        String.to_float(x)
      else
        String.to_integer(x)
      end
    end)
    |> List.to_tuple()
  end

  @spec parse_coord_block(s :: String.t()) :: [coord()]
  defp parse_coord_block(s) do
    s
    |> String.trim()
    |> String.split("\n")
    |> Enum.map(&parse_coords/1)
  end

  @typep some_coord :: coord() | [coord()]
  @typep xmlElem :: SweetXml.xmlElement()
  @typep placemark :: %{name: String.t(), coords: some_coord()}
  @spec parse_placemark(xmlElem(), (String.t() -> some_coord())) :: placemark()
  defp parse_placemark(elem, p_coords) do
    elem
    |> xpath(
      ~x"//Placemark"l,
      name: ~x"./name/text()" |> transform_by(&List.to_string/1),
      coords:
        ~x"//coordinates/text()"
        |> transform_by(fn s -> s |> List.to_string() |> p_coords.() end)
    )
  end

  @typep station_map :: %{coord() => Station.t()}
  @spec merge_lines_stations([placemark()], station_map) :: [Line.t()]
  defp merge_lines_stations(lines, stations) do
    lines
    |> Enum.map(fn line ->
      %Line{
        name: line.name,
        stations:
          line.coords
          |> Enum.map(fn c -> stations[c] end)
          |> Enum.filter(fn s -> not is_nil(s) end)
      }
    end)
  end

  @spec metro_system(String.t()) :: [Line.t()]
  def metro_system(filepath) do
    [lines_elem, stations_elem, _others] =
      filepath
      |> File.read!()
      |> xpath(~x"//Folder"l)

    stations =
      stations_elem
      |> parse_placemark(&parse_coords/1)
      |> Enum.map(fn p -> {p.coords, struct(Station, p)} end)
      |> Map.new()

    lines_elem
    |> parse_placemark(&parse_coord_block/1)
    |> merge_lines_stations(stations)
  end

  @spec metro_network(String.t()) :: Graph.t()
  def metro_network(filepath) do
    metro = metro_system(filepath)

    connections =
      Enum.map(metro, fn line ->
        case line.stations do
          [s1 | [s2 | ss]] ->
            Enum.reduce(ss, [{s1.name, s2.name}], fn s, l = [{_, s0} | _] ->
              [{s0, s.name} | [{s.name, s0} | l]]
            end)
            |> Enum.map(&Tuple.append(&1, label: line.name))

          _ ->
            []
        end
      end)
      |> List.flatten()

    Graph.new() |> Graph.add_edges(connections)
  end

  @spec query(String.t(), String.t(), String.t()) :: [Segment.t()]
  def query(filepath, source, dest) do
    m = metro_network(filepath)

    path = Graph.get_shortest_path(m, source, dest)

    if is_nil(path) do
      nil
    else
      path
      |> Enum.zip(Enum.drop(path, 1))
      |> Enum.map(fn {s, d} -> m |> Graph.edges(s, d) |> hd() end)
      |> Bunsan.MyList.group_by(fn e -> e.label end)
      |> Enum.map(fn {line, tracks} ->
        %Segment{
          line: line,
          init: tracks |> hd() |> Map.get(:v1),
          last: tracks |> List.last() |> Map.get(:v2),
          len: length(tracks)
        }
      end)
    end
  end
end
