defmodule Bunsan.SUT do
  @moduledoc false

  def sum(a, b), do: a + b
end
