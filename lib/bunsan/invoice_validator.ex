defmodule Bunsan.InvoiceValidator do
  @moduledoc false

  Calendar.put_time_zone_database(Tzdata.TimeZoneDatabase)

  @valid_past_seconds 72 * 60 * 60
  @future_slack_seconds 5 * 60
  @valid_time_range Range.new(-@valid_past_seconds, @future_slack_seconds)

  @spec valid_time_range() :: Range.t()
  def valid_time_range, do: @valid_time_range

  defp mem(x, xs), do: Enum.member?(xs, x)

  @spec validate_dates(DateTime.t(), DateTime.t()) :: boolean()
  def validate_dates(billing_date, curr_date) do
    billing_date
    |> DateTime.shift_zone!("Etc/UTC")
    |> DateTime.diff(DateTime.shift_zone!(curr_date, "Etc/UTC"), :second)
    |> mem(valid_time_range())
  end
end
