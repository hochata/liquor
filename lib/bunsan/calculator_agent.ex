defmodule CalculatorAgent do
  @moduledoc false

  use Agent

  def calc(pid, {:sum, val}), do: Agent.update(pid, &(&1 + val))
  def calc(pid, {:mult, val}), do: Agent.update(pid, &(&1 * val))
  def calc(pid, {:sub, val}), do: Agent.update(pid, &(&1 - val))
  def calc(pid, {:div, val}), do: Agent.update(pid, &(&1 / val))
  def calc(_pid, {_noop, _val}), do: :noop

  def state(pid), do: Agent.get(pid, & &1)

  def init(n \\ 0) do
    Agent.start(fn -> n end)
  end
end
