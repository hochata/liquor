defmodule Bunsan.Macros do
  @moduledoc false

  defmacro nf(name, do: body) do
    quote do
      def unquote(name)(), do: unquote(body)
    end
  end

  defmodule Machine do
    @moduledoc false

    states = [
      accept: {:zero, :accept},
      accept: {:one, :reject},
      reject: {:zero, :reject},
      reject: {:one, :accept}
    ]

    for {curr_state, {input, next_state}} <- states do
      def unquote(input)(unquote(curr_state)) do
        unquote(next_state)
      end
    end

    def init, do: :accept
  end

  defmodule Github do
    @moduledoc false

    @user "eqf0"

    "https://api.github.com/users/#{@user}/repos"
    |> Tesla.get!(headers: ["User-Agent": @user])
    |> Map.get(:body)
    |> Jason.decode!()
    |> Enum.each(fn repo ->
      def unquote(String.to_atom(repo["name"]))() do
        unquote(Macro.escape(repo))
      end
    end)

    def go(repo) do
      url = apply(__MODULE__, repo, [])["html_url"]
      IO.puts("Launching browser to #{url}...")
      System.cmd("xdg-open", [url])
    end
  end
end
