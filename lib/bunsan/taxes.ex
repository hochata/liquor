defmodule Bunsan.Pragmatic do
  @moduledoc false

  def add_tax(order, tax_rates) do
    tax = Map.get(tax_rates, order.ship_to, 0)
    Map.put(order, :total_amount, order.net_amount * (1 + tax))
  end

  def add_all_taxes(orders, tax_rates) do
    Enum.map(orders, fn o -> add_tax(o, tax_rates) end)
  end
end
