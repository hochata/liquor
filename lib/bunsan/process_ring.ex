defmodule ProcessRing do
  @moduledoc false

  def init_link(n) when n > 0 do
    next = spawn(__MODULE__, :init_link, [n - 1, self()])
    wait_for_message(next)
  end

  def init_link(n, pid0) when n > 0 and is_pid(pid0) do
    next = spawn(__MODULE__, :init_link, [n - 1, pid0])
    wait_for_message(next)
  end

  def init_link(0, pid0) when is_pid(pid0) do
    wait_for_message(pid0, :last)
  end

  defp next_mess_content(curr_round, goal_rounds, type) do
    case {type, curr_round < goal_rounds} do
      {:last, false} -> :noop
      {:last, true} -> curr_round + 1
      _ -> curr_round
    end
  end

  defp wait_for_message(next, type \\ :internal) do
    receive do
      {:pass_it_on, mess, curr, goal} ->
        IO.puts("Process #{inspect(self())} received #{mess}, round #{curr}")

        case next_mess_content(curr, goal, type) do
          :noop -> :noop
          n when is_number(n) -> send(next, {:pass_it_on, mess, n, goal})
        end
    end

    wait_for_message(next, type)
  end

  def rounds(ring, mess, n) do
    send(ring, {:pass_it_on, mess, 1, n})
  end

  def init(n) do
    spawn(__MODULE__, :init_link, [n])
  end
end
