defmodule Bunsan.Wordcount do
  @moduledoc false

  def prepare_text(text) do
    text
    |> String.downcase()
    |> String.normalize(:nfd)
    |> String.replace(~r/[[:punct:]]/, " ")
    |> String.replace(~r/[^\\0-\x7f^\s]/, "")
  end

  def red_count(lines) do
    Enum.reduce(lines, %{}, &Map.update(&2, &1, 1, fn n -> n + 1 end))
  end

  def red_count_async(lines) do
    lines
    |> Enum.chunk_every(1_000)
    |> Enum.map(&Task.async(fn -> red_count(&1) end))
    |> Enum.reduce(
      %{},
      &(Task.await(&1)
        |> Map.merge(&2, fn _k, v1, v2 ->
          v1 + v2
        end))
    )
  end

  def count(path) do
    path
    |> File.read!()
    |> prepare_text()
    |> String.split()
    |> red_count()
  end

  def count_async(path) do
    path
    |> File.read!()
    |> prepare_text()
    |> String.split()
    |> red_count_async()
  end
end
