defmodule Calculator do
  @moduledoc false

  defp apply_op(:sum, a, b), do: a + b
  defp apply_op(:mult, a, b), do: a * b
  defp apply_op(:sub, a, b), do: a - b
  defp apply_op(:div, a, b), do: a / b
  defp apply_op(_op, a, _b), do: a

  def init(n \\ 0) do
    s =
      receive do
        {op, m, pid} when is_atom(op) and is_number(m) and is_pid(pid) ->
          s = apply_op(op, n, m)
          send(pid, {:state, s})
          s

        _ ->
          n
      end

    init(s)
  end
end
