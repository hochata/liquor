defmodule GE.Gen do
  @moduledoc false

  use PropCheck

  def choose_exactly(l) do
    l
    |> Enum.map(&exactly/1)
    |> union()
  end

  def sized_arith_term(s) when s <= 0, do: nat()

  def sized_arith_term(s) when s > 0 do
    let [
      n <- integer(0, s - 1),
      l <- sized_arith_term(^n),
      r <- sized_arith_term(s - ^n - 1),
      op <- choose_exactly([:+, :-, :*]),
    ] do
      {
        op,
        [],
        [l, r]
      }
    end
  end

  def arith_term() do
    sized(s, sized_arith_term(s))
  end
end
