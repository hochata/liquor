defmodule GE.Grammar do
  defp reduce_ast(grammar, rule, prev_term) do
    with {:ok, terms, gene} <- prev_term,
         {:ok, term, rem_gene} <- to_ast(grammar, gene, rule) do
      {:ok, [term | terms], rem_gene}
    end
  end

  def to_ast(grammar, [], rule) when is_map_key(grammar, rule) do
    {:error, :insufficient_codons}
  end

  def to_ast(grammar, [codon | rest], rule) when is_map_key(grammar, rule) do
    prods = grammar[rule]

    case Enum.at(prods, rem(codon, length(prods))) do
      {op_rule, _, args_rules} ->
        {:ok, terms, _} =
          Enum.reduce(
            [op_rule | args_rules],
            {:ok, [], rest},
            &reduce_ast(grammar, &1, &2)
          )

        [op | args] = Enum.reverse(terms)

        {:ok, {op, [], args}}

      single_rule ->
        to_ast(grammar, rest, single_rule)
    end
  end

  def to_ast(_, gene, rule), do: {:ok, rule, gene}

  def arith_term(gene) do
    grammar = %{
      s: [:num, {:op, [], [:num, :num]}],
      num: [0, 1, 2, 3, 4],
      op: [:+, :-]
    }

    to_ast(grammar, gene, :s)
  end
end
