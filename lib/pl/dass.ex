defmodule Liquor.PL.Dass do
  @moduledoc false

  def ast_collect({atom, _, args} = ast, atom) do
    [ast | ast_collect(args, atom)]
  end

  def ast_collect({_, _, args}, atom) when is_list(args) do
    Enum.flat_map(args, &ast_collect(&1, atom))
  end

  def ast_collect([do: ast], atom), do: ast_collect(ast, atom)

  def ast_collect(args, atom) when is_list(args) do
    Enum.flat_map(args, &ast_collect(&1, atom))
  end

  def ast_collect(_, _), do: []

  def bytes(module) do
    :code.get_object_code(module)
    |> elem(1)
    |> :beam_disasm.file()
  end

  def __after_compile__(_a, b) do
    b
    |> :beam_disasm.file()
    |> elem(5)
    |> Enum.map(fn a ->
      a
      |> elem(4)
      |> Enum.at(2)
    end)
  end
end
