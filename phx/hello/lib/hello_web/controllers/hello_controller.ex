defmodule HelloWeb.HelloController do
  use HelloWeb, :controller

  def world(c, %{"name" => n}) do
    render(c, "world.html", name: n)
  end
end
