defmodule RumblWeb.UserController do
  use RumblWeb, :controller

  alias Rumbl.Accounts

  @spec index(Plug.Conn.t(), map) :: Plug.Conn.t()
  def index(c, _params) do
    render(c, "index.html", users: Accounts.list_users())
  end

  @spec show(Plug.Conn.t(), map) :: Plug.Conn.t()
  def show(c, %{"id" => id}) do
    render(c, "show.html", user: Accounts.get_user(id))
  end
end
