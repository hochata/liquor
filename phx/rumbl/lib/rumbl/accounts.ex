defmodule Rumbl.Accounts do
  @moduledoc """
  Accounts context
  """

  defmodule User do
    defstruct [:id, :name, :username]
  end

  def list_users() do
    [
      %User{id: "0", name: "José", username: "josevalim"},
      %User{id: "1", name: "Bruce", username: "redrapids"},
      %User{id: "2", name: "Chirs", username: "chrismccord"}
    ]
  end

  def get_user(id) do
    list_users() |> Enum.find(&(&1.id == id))
  end

  def get_user_by(params) do
    list_users() |> Enum.find(&Enum.all?(params, fn {k, v} ->
      Map.get(&1, k) == v
    end))
  end
end
