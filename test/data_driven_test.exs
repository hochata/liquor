defmodule Bunsan.DataDrivenTest do
  use ExUnit.Case

  data = [
    {1, 3, 4},
    # Error intencional
    {7, 4, 11},
    {9, 1, 10},
    {-5, 5, 0},
    # Error intencional
    {-10, -20, -30}
  ]

  for {a, b, c} <- data do
    @a a
    @b b
    @c c
    test "sum of #{@a} and #{@b} should equal #{@c}" do
      assert Bunsan.SUT.sum(@a, @b) == @c
    end
  end
end
