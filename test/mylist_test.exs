defmodule Bunsan.MyListTester do
  use ExUnit.Case
  use PropCheck

  alias Bunsan.MyList, as: L

  doctest Bunsan.MyList

  describe "reduce" do
    property "can do arithmetic" do
      forall [l <- list(nat()), f <- function([nat(), nat()], nat())] do
        assert L.reduce(l, 0, f) == Enum.reduce(l, 0, f)
      end
    end
  end

  describe "map" do
    property "can map" do
      forall [l <- list(any()), f <- function([any()], any())] do
        assert L.map(l, f) == Enum.map(l, f)
      end
    end
  end

  describe "each" do
    property "doesn't crash" do
      forall [l <- list(any()), f <- function([any()], any())] do
        assert L.each(l, f) == :ok
      end
    end
  end

  describe "zip" do
    property "can zip" do
      forall [l0 <- list(any()), l1 <- list(any())] do
        assert L.zip(l0, l1) == Enum.zip(l0, l1)
      end
    end
  end

  describe "zip_with" do
    property "can apply function correctly" do
      forall [
        l0 <- list(any()),
        l1 <- list(any()),
        f <- function([any(), any()], any())
      ] do
        assert L.zip_with(l0, l1, f) == Enum.zip_with(l0, l1, f)
      end
    end
  end
end
