defmodule Bunsan.InvoiceValidatorTest do
  use ExUnit.Case
  use PropCheck

  import Bunsan.InvoiceValidator

  Calendar.put_time_zone_database(Tzdata.TimeZoneDatabase)

  def choose_from(l) do
    l
    |> Enum.map(&PropCheck.BasicTypes.exactly/1)
    |> union()
  end

  def time_zone() do
    Tzdata.zone_list()
    |> choose_from()
  end

  def shift({d1, d2}, {z1, z2}), do: {shift(d1, z1), shift(d2, z2)}

  def shift(d, z), do: DateTime.shift_zone!(d, z)

  def validate_dates({a, b}), do: validate_dates(a, b)

  def prepare_dates(separation, :hours) do
    prepare_dates(separation * 60 * 60, :seconds)
  end

  def prepare_dates(separation, :minutes) do
    prepare_dates(separation * 60, :seconds)
  end

  def prepare_dates(separation, :seconds) do
    n = DateTime.utc_now()
    m = DateTime.add(n, separation, :second)

    {m, n}
  end

  def valid_diff?(diff_gen, unit) do
    forall [i <- diff_gen, t1 <- time_zone(), t2 <- time_zone()] do
      assert(prepare_dates(i, unit) |> shift({t1, t2}) |> validate_dates())
    end
  end

  def invalid_diff?(diff_gen, unit) do
    forall [i <- diff_gen, t1 <- time_zone(), t2 <- time_zone()] do
      assert(!(prepare_dates(i, unit) |> shift({t1, t2}) |> validate_dates()))
    end
  end

  describe "alma's" do
    common_cap =
      DateTime.from_naive!(
        ~N[2022-03-23 15:06:35],
        "America/Mexico_City"
      )

    cases = %{
      "72 hrs" => [
        {"America/Tijuana", ~N[2022-03-20 13:06:31], false},
        {"America/Mazatlan", ~N[2022-03-20 14:06:31], false},
        {"America/Mexico_City", ~N[2022-03-20 15:06:31], false},
        {"America/Cancun", ~N[2022-03-20 16:06:31], false},
        # faulty
        {"America/Tijuana", ~N[2022-03-20 13:06:35], true},
        {"America/Mazatlan", ~N[2022-03-20 14:06:35], true},
        {"America/Mexico_City", ~N[2022-03-20 15:06:35], true},
        {"America/Cancun", ~N[2022-03-20 16:06:35], true}
      ],
      "5 mns" => [
        {"America/Tijuana", ~N[2022-03-23 13:11:35], true},
        {"America/Mazatlan", ~N[2022-03-23 14:11:35], true},
        {"America/Mexico_City", ~N[2022-03-23 15:11:35], true},
        {"America/Cancun", ~N[2022-03-23 16:11:35], true},
        # faulty
        {"America/Tijuana", ~N[2022-03-23 13:11:36], false},
        {"America/Mazatlan", ~N[2022-03-23 14:11:36], false},
        {"America/Mexico_City", ~N[2022-03-23 15:11:36], false},
        {"America/Cancun", ~N[2022-03-23 16:11:36], false}
      ]
    }

    for {type, test_cases} <- cases,
        {tz, n_date, st} <- test_cases do
      test "#{type}: #{n_date} #{tz} at #{common_cap} is #{st}" do
        date =
          DateTime.from_naive!(
            unquote(Macro.escape(n_date)),
            unquote(Macro.escape(tz))
          )

        other = unquote(Macro.escape(common_cap))
        assert unquote(st) == validate_dates(date, other)
      end
    end
  end

  describe "validate dates with limit values" do
    test "instantaneous dates are valid" do
      now = DateTime.utc_now()
      assert validate_dates(now, now)
    end

    test "exactly five minutes into the future is accepted" do
      assert prepare_dates(5, :minutes) |> validate_dates()
    end

    test "exactly six minutes into the future is rejected" do
      refute prepare_dates(6, :minutes) |> validate_dates()
    end

    test "exactly 72 hours into the past is accepted" do
      assert prepare_dates(-72, :hours) |> validate_dates()
    end

    test "exactly 73 hours into the past is rejected" do
      refute prepare_dates(-73, :hours) |> validate_dates()
    end
  end

  describe "validate dates with random timezones" do
    property "up to 5 minutes into the future are valid" do
      valid_diff?(integer(0, 5), :minutes)
    end

    property "more than 5 minutes into the future are rejected" do
      invalid_diff?(integer(6, :inf), :minutes)
    end

    property "up to 72 hours into the past are valid" do
      valid_diff?(integer(-72, 0), :hours)
    end

    property "more than 72 hours into the past are rejected" do
      invalid_diff?(integer(:inf, -73), :hours)
    end
  end
end
