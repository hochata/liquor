defmodule Bunsan.FizzBuzzTester do
  use ExUnit.Case
  use PropCheck

  import Bunsan.FizzBuzz

  doctest Bunsan.FizzBuzz

  describe "fizzbuzz" do
    property "3n gives fizz" do
      forall n <- nat() do
        fizz = fizzbuzz_n(3 * n)
        assert(String.starts_with?(fizz, "fizz"))
      end
    end

    property "5n gives buzz" do
      forall n <- nat() do
        buzz = fizzbuzz_n(5 * n)
        assert(String.ends_with?(buzz, "buzz"))
      end
    end

    property "15n gives fizzbuzz" do
      forall n <- nat() do
        assert fizzbuzz_n(15 * n) == "fizzbuzz"
      end
    end

    property "15n+1 gives 15n+1" do
      forall n <- nat() do
        assert fizzbuzz_n(15 * n + 1) == 15 * n + 1
      end
    end

    property "either 3n-1 or 3n+1 gives itself" do
      forall n <- nat() do
        m = 3 * n

        case rem(m + 1, 5) do
          0 -> assert fizzbuzz_n(m - 1) == m - 1
          _ -> assert fizzbuzz_n(m + 1) == m + 1
        end
      end
    end
  end
end
