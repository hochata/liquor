-- Consultas
-- Crear consultas para los siguiente casos
-- 1. Obtener la lista de usuarios (name, handle, email) que están activas (enabled) y no tienen imagen de perfil, ordenados alfabéticamente por nombre

SELECT name, handle, email
FROM users
WHERE users.image IS NULL AND users.enabled
ORDER BY name;



-- #+RESULTS:
-- | name                            | handle              | email                       |
-- |---------------------------------+---------------------+-----------------------------|
-- | Adrian Alberto Guzman Sanchez   | Adrian2122          | adrian.guzman@bunsan.io     |
-- | Adrián Reyes Delgado            | adrianRD            | adrian.reyes@bunsan.io      |
-- | Alberto Tellez                  | Alberto             | alberto@bunsan.io           |
-- | Aldo Flores Aparicio            | aldofa              | aldo.flores@bunsan.io       |
-- | Andrew González                 | andrewGM            | andrew@bunsan.io            |
-- | Daniel Ortíz Sánchez            | DanielO             | daniel.ortiz@bunsan.io      |
-- | David Alexis Romo Jáuregui      | David Romo          | david.romo@bunsan.io        |
-- | Gerardo Aquino                  | java.daba.doo       | gerardo@bunsan.io           |
-- | Humberto Bañuelos Flores        | hbanuelos           | humberto.banuelos@bunsan.io |
-- | Héctor González Olmos           | Santo               | hector.gonzalez@bunsan.io   |
-- | Israel Salinas Hernández        | Israel Salinas hdez | israel.salinas@bunsan.io    |
-- | Ivan Barron                     | ivandavid77         | david@bunsan.io             |
-- | Jesús Jiménez Cordero           | Jesus.JimenezC      | jesus.jimenez@bunsan.io     |
-- | Josafat Rosas Ortiz             | JRO                 | josafat.rosas@bunsan.io     |
-- | Juan Carlos Basurto Martínez    | Carlos Basurto      | carlos.basurto@bunsan.io    |
-- | Juan Galicia                    | ga_c                | juan.galicia@bunsan.io      |
-- | Juan Manuel Viscencio Ramirez   | mviscencio          | juan.viscencio@bunsan.io    |
-- | Lorena Mireles                  | Lorena              | lorena@bunsan.io            |
-- | Luis Enrique Sastré García      | Luis Sastré         | luis.sastre@bunsan.io       |
-- | Manuel de Jesus Urias Rodriguez | murias_bunsan       | manuel.urias@bunsan.io      |
-- | Noberto Ortigoza                | hiphoox             | norberto@bunsan.io          |

-- 2. Obtener la lista de usuarios (name, handle) que están activos, saben elixir al menos en un nivel competent y además saben aws en cualquier nivel.

(SELECT users.name, users.handle
 FROM (
   (users INNER JOIN
     (SELECT user_skills.user_id, user_skills.skill_id
      FROM user_skills
      WHERE user_skills.level IN ('competent', 'proficient', 'expert')) AS user_skills
   ON users.id = user_skills.user_id)
   INNER JOIN
   (SELECT skills.id
    FROM skills
    WHERE skills.name = 'Elixir') AS skills
   ON user_skills.skill_id = skills.id)
 WHERE users.enabled)
INTERSECT
(SELECT users.name, users.handle
 FROM (
   (users INNER JOIN
     (SELECT user_skills.user_id, user_skills.skill_id
      FROM user_skills) AS user_skills
   ON users.id = user_skills.user_id)
   INNER JOIN
   (SELECT skills.id
    FROM skills
    WHERE skills.name = 'AWS') AS skills
   ON user_skills.skill_id = skills.id)
 WHERE users.enabled);


-- #+RESULTS:
-- | name                    | handle         |
-- |-------------------------+----------------|
-- | Juan Galicia            | ga_c           |
-- | Agustín Ramos           | MachinesAreUs  |
-- | Noé Isai Pérez Chamorro | Noe Isai Perez |
-- | David Valencia          | dvidvlencia    |
-- | Misael Pérez Chamorro   | misaelpc       |
-- | Misael Ramirez Lopez    | MisaelRaLo     |

-- 3. Obtener la lista de los 10 usuarios activos (name, handle) que son los que tienen menos skills registrados, ordenados de menor a mayor.

SELECT users.name, users.handle, COUNT(user_skills.skill_id)
FROM (users INNER JOIN user_skills ON users.id = user_skills.user_id)
GROUP BY users.id
HAVING users.enabled
ORDER BY COUNT(user_skills.skill_id)
LIMIT 10;


-- #+RESULTS:
-- | name                              | handle                  | count |
-- |-----------------------------------+-------------------------+-------|
-- | Karla Emma Loya Alvarado          | KarlaL                  |     1 |
-- | Roberto Javier Sánchez Urrutia    | RobSanUrr               |     1 |
-- | Adrian Alberto Guzman Sanchez     | Adrian2122              |     1 |
-- | Jose Antonio Serrano Peña         | Antonio.serrano.bunsan1 |     1 |
-- | Uriel Alberto Gembe Campanur      | Uriel Gembe             |     1 |
-- | Montserrat Anabel Cifuentes Reyes | Annabel                 |     1 |
-- | Daniela González                  | Daniela.                |     1 |
-- | Marduk Perez de Lara              | Marduk                  |     1 |
-- | Ricardo Alfonso González Téllez   | Ricardo G               |     1 |
-- | Sergio Acosta                     | sergio.acosta           |     1 |

-- 4. En base a las asignaciones vigentes, calcular la fecha en la que al menos la mitad de los usuarios activos ya no tienen trabajo asignado.

SELECT PERCENTILE_DISC(0.5) WITHIN GROUP (ORDER BY end_date)
FROM assignments
WHERE (DATE '2022-03-01', DATE '2022-03-31') OVERLAPS (start_date, end_date);



-- #+RESULTS:
-- | percentile_disc |
-- |-----------------|
-- |      2022-05-31 |

-- 5. En base a las asignaciones vigentes en marzo y considerando que cada día laborable (lun-vie) es de 8 hrs, calcular el total de horas trabajadas durante marzo por todos los usuarios.

SELECT COUNT(DAYS.d) * 8 AS total_hours
FROM
 (SELECT GENERATE_SERIES(start_date, end_date, '1 DAY'::INTERVAL) d
  FROM assignments) DAYS
WHERE DAYS.d
 BETWEEN '2022-03-01'::DATE AND '2022-03-31'::DATE
 AND EXTRACT(DOW FROM DAYS.d) NOT IN (0, 6);



-- #+RESULTS:
-- | total_hours |
-- |-------------|
-- |       10752 |

SELECT * FROM GENERATE_SERIES(1, 1);
