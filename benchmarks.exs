lines =
  "bible.txt"
  |> File.read!()
  |> Bunsan.Wordcount.prepare_text()
  |> String.split()

Benchee.run(%{
      "red_count" => fn -> Bunsan.Wordcount.red_count(lines) end,
      "red_count_async" => fn -> Bunsan.Wordcount.red_count_async(lines) end
})
