defmodule Liquor.MixProject do
  use Mix.Project

  def project do
    [
      app: :liquor,
      version: "0.1.0",
      elixir: "~> 1.13",
      start_permanent: Mix.env() == :prod,
      deps: deps(),
      test_coverage: [tool: ExCoveralls],
      preferred_cli_env: [coveralls: :test]
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger]
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:excoveralls, "~> 0.3", only: :test},
      {:propcheck, "~> 1.4", only: [:test, :dev]},
      {:sweet_xml, "~> 0.7.1"},
      {:libgraph, "~> 0.13.3"},
      {:tesla, "~> 1.4"},
      {:jason, "~> 1.2"},
      {:tzdata, "~> 1.1"},
      {:credo, "~> 1.6", only: [:dev, :test], runtime: false},
      {:datalog, "~> 2.0"},
      {:benchee, "~> 1.0", only: :dev},
      {:amqp, "~> 1.0"},
    ]
  end
end
